package com.bottlerocketstudios.mobile.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat

class ShoppingCartFragment : Fragment(), CartAdapter.Listener {

    private val avm: NavigationTestAvm by navGraphViewModels(R.id.nav_graph)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_shopping_cart, container, false)

        val cartAdapter = CartAdapter(this)
        val recyclerView: RecyclerView = root.findViewById(R.id.product_list)
        val subtotal: TextView = root.findViewById(R.id.subtotal)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = cartAdapter

        avm.getLiveCart().observe(this, Observer { productList ->
            if (productList != null) {
                cartAdapter.updateProducts(productList)
                val total = productList.sumByDouble { it.price }
                subtotal.text = NumberFormat.getCurrencyInstance().format(total)
            }
        })

        val checkout: View = root.findViewById(R.id.checkout)
        checkout.setOnClickListener {
            if (avm.getLiveCart().value?.size ?: 0 > 0) {
                checkout.findNavController().navigate(R.id.paymentFragment)
            } else {
                checkout.findNavController().navigate(ShoppingCartFragmentDirections.actionShoppingCartFragmentToErrorDialog("No items in cart"))
            }
        }

        return root
    }

    override fun itemRemovedFromCart(product: Product) = avm.itemRemovedFromCart(product)
}
