package com.bottlerocketstudios.mobile.navigationtest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat

class ProductAdapter(private val products: List<Product>, private val listener: Listener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.shopping_item_layout, parent, false), listener)
    }

    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            products.getOrNull(position)?.let {
                holder.bind(it)
            }
        }
    }

    interface Listener {
        fun itemAddedToCart(product: Product)
    }

    private class ViewHolder(private val root: View, private val listener: Listener): RecyclerView.ViewHolder(root) {
        private val name: TextView = root.findViewById(R.id.item_name)
        private val price: TextView = root.findViewById(R.id.item_price)
        private val add: View = root.findViewById(R.id.item_add)

        fun bind(product: Product) {
            name.text = product.name
            price.text = NumberFormat.getCurrencyInstance().format(product.price)

            root.setOnClickListener {
                root.findNavController().navigate(ShoppingFragmentDirections.productDetail(product))
            }

            add.setOnClickListener {
                listener.itemAddedToCart(product)
            }
        }
    }
}