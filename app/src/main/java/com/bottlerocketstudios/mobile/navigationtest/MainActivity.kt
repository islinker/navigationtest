package com.bottlerocketstudios.mobile.navigationtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI.*
import com.google.android.material.navigation.NavigationView



class MainActivity : AppCompatActivity() {

    private var navigationController: NavController? = null
    private var drawerLayout: DrawerLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigationController = findNavController(R.id.nav_host_fragment)
        drawerLayout = findViewById(R.id.drawer_layout)

        val navigationView: NavigationView = findViewById(R.id.nav_view)
        val toolbar: Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)

        navigationController?.let {
            setupWithNavController(toolbar, it, drawerLayout)
            setupWithNavController(navigationView, it)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return if (navigationController?.currentDestination?.id == R.id.confirmationFragment) {
            navigationController?.navigate(R.id.action_global_homeFragment)
            true
        } else {
            navigationController?.navigateUp() ?: false
        }
    }

    override fun onBackPressed() {
        if (drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            drawerLayout?.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (navigationController?.currentDestination?.id != R.id.confirmationFragment) {
            menuInflater.inflate(R.menu.app_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item) || navigationController?.run { onNavDestinationSelected(item, this) } ?: false

    }

}
