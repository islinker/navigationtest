package com.bottlerocketstudios.mobile.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.navGraphViewModels
import java.text.NumberFormat

class ProductDetailsFragment : Fragment() {

    private var product: Product? = null
    private val avm: NavigationTestAvm by navGraphViewModels(R.id.nav_graph)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root =  inflater.inflate(R.layout.fragment_product_details, container, false)

        try {
            product = arguments?.run { ProductDetailsFragmentArgs.fromBundle(this).product } ?: throw IllegalStateException("No product")
        } catch (e: Exception) {
            //If we don't have a valid product close the screen
            root.findNavController().popBackStack()
        }

        val image: ImageView = root.findViewById(R.id.product_image)
        val name: TextView = root.findViewById(R.id.product_name)
        val price: TextView = root.findViewById(R.id.product_price)
        val add: View = root.findViewById(R.id.product_add)

        product?.let {
            image.setImageResource(it.image)
            name.text = it.name
            price.text = NumberFormat.getCurrencyInstance().format(it.price)

            add.setOnClickListener { _ ->
                avm.itemAddedToCart(it)
            }
        }

        return root
    }

}
