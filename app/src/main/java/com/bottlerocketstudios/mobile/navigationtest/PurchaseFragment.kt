package com.bottlerocketstudios.mobile.navigationtest


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController

class PurchaseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_purchase, container, false)

        val purchase: View = root.findViewById(R.id.purchase)
        purchase.setOnClickListener {
            purchase.findNavController().navigate(R.id.confirmationFragment)
        }

        return root
    }

}
