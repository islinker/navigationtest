package com.bottlerocketstudios.mobile.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ShoppingFragment : Fragment(), ProductAdapter.Listener {

    private val avm: NavigationTestAvm by navGraphViewModels(R.id.nav_graph)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_shopping, container, false)

        val products: List<Product> = listOf(
                Product("Really Cool Thing", 59.99, R.drawable.blob_timm_duck),
                Product("Kinda Cool Thing", 39.99, R.drawable.blob_timm_happy),
                Product("Way Overpriced Thing", 99.99, R.drawable.blob_timm_sad),
                Product("What Even Is This Thing", 9.99, R.drawable.blob_timm_sweat)
        )

        val productAdapter = ProductAdapter(products, this)
        val recyclerView: RecyclerView = root.findViewById(R.id.product_list)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = productAdapter

        return root
    }

    override fun itemAddedToCart(product: Product) = avm.itemAddedToCart(product)
}
