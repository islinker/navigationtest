package com.bottlerocketstudios.mobile.navigationtest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat

class CartAdapter(private val listener: Listener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var products: List<ListableItem>? = null

    fun updateProducts(newProducts: List<Product>) {
        runDiffUtilsDispatch(getListableItems(newProducts))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cart_item_layout, parent, false), listener)
    }

    override fun getItemCount(): Int = products?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            products?.getOrNull(position)?.let {
                holder.bind(it.item as Product)
            }
        }
    }

    private fun getListableItems(newProducts: List<Product>): List<ListableItem> {
        return newProducts.map{ CartItem(it) }.toList()
    }

    private fun runDiffUtilsDispatch(newItems: List<ListableItem>) {
        val diffResult = DiffUtil.calculateDiff(ListableItemDiffUtilCallback(products, newItems), false)
        diffResult.dispatchUpdatesTo(this)
        products = newItems
    }

    interface Listener {
        fun itemRemovedFromCart(product: Product)
    }

    private class ViewHolder(private val root: View, private val listener: Listener): RecyclerView.ViewHolder(root) {
        private val name: TextView = root.findViewById(R.id.item_name)
        private val price: TextView = root.findViewById(R.id.item_price)
        private val remove: View = root.findViewById(R.id.item_remove)

        fun bind(product: Product) {
            name.text = product.name
            price.text = NumberFormat.getCurrencyInstance().format(product.price)

            root.setOnClickListener {
                root.findNavController().navigate(ShoppingFragmentDirections.productDetail(product))
            }

            remove.setOnClickListener {
                listener.itemRemovedFromCart(product)
            }
        }
    }
}