package com.bottlerocketstudios.mobile.navigationtest

class CartItem(private val product: Product) : ListableItem() {

    override val itemViewType = 0

    override val item: Any?
        get() = product

    override fun isTheSame(item: ListableItem): Boolean {
        if (item !is CartItem) return false
        if (item.item == null) return false
        return item == this.item
    }

    override fun getChangesComparedTo(oldItem: ListableItem): Any? {
        if (item !is CartItem) return null
        val oldItemContents = oldItem.item as? Product ?: return null

        return when {
            oldItemContents.name != product.name -> product.name
            oldItemContents.image != product.image -> product.image
            oldItemContents.price != product.price -> product.price
            else -> null
        }
    }
}