package com.bottlerocketstudios.mobile.navigationtest

import androidx.annotation.DrawableRes
import java.io.Serializable

data class Product(val name: String, val price: Double, @DrawableRes val image: Int): Serializable