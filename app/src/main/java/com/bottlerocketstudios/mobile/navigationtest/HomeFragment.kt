package com.bottlerocketstudios.mobile.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation

class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)

        val productsButton: Button = root.findViewById(R.id.products_button)
        productsButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.shopping))

        return root
    }

}
