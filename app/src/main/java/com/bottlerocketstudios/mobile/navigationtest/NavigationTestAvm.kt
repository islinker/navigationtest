package com.bottlerocketstudios.mobile.navigationtest

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavigationTestAvm: ViewModel() {

    private val liveCart: MutableLiveData<List<Product>> = MutableLiveData()

    fun getLiveCart(): LiveData<List<Product>> = liveCart

    fun itemAddedToCart(product: Product) {
        val currentList = liveCart.value?.toMutableList() ?: mutableListOf()
        currentList.add(product)
        liveCart.value = currentList
    }

    fun itemRemovedFromCart(product: Product) {
        val currentList = liveCart.value?.toMutableList() ?: mutableListOf()
        currentList.remove(product)
        liveCart.value = currentList
    }
}