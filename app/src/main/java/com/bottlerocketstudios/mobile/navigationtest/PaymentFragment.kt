package com.bottlerocketstudios.mobile.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController


class PaymentFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_payment, container, false)

        val review: View = root.findViewById(R.id.review)
        review.setOnClickListener {
            review.findNavController().navigate(R.id.purchaseFragment)
        }

        return root
    }

}
