package com.bottlerocketstudios.mobile.navigationtest

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil

/**
 * See https://medium.com/@nullthemall/diffutil-is-a-must-797502bc1149#.5quweyq65 for some good
 * reading about DiffUtil
 */
class ListableItemDiffUtilCallback(private val oldList: List<ListableItem>?, private val newList: List<ListableItem>?) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList?.size ?: 0
    }

    override fun getNewListSize(): Int {
        return newList?.size ?: 0
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldListItem = oldList?.getOrNull(oldItemPosition)
        val newListItem = newList?.getOrNull(newItemPosition)

        return if (oldListItem != null && newListItem != null) {
            oldListItem.isTheSame(newListItem)
        } else {
            false
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldListItem = oldList?.getOrNull(oldItemPosition)
        val newListItem = newList?.getOrNull(newItemPosition)

        return if (oldListItem != null && newListItem != null) {
            oldListItem.equals(newListItem)
        } else {
            false
        }
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldListItem = oldList?.getOrNull(oldItemPosition)
        val newListItem = newList?.getOrNull(newItemPosition)

        return if (oldListItem != null && newListItem != null) {
            newListItem.getChangesComparedTo(oldListItem)
        } else {
            null
        }
    }
}