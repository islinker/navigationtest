package com.bottlerocketstudios.mobile.navigationtest

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class ErrorDialog: DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity as Context)
        val inflater = activity?.layoutInflater
        val view = inflater?.inflate(R.layout.error_dialog, null)
        val message: TextView? = view?.findViewById(R.id.error_dialog_message)
        val messageText = arguments?.run { ErrorDialogArgs.fromBundle(this).message }
        message?.text = messageText

        return builder.setView(view)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }.create()
    }
}