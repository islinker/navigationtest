package com.bottlerocketstudios.mobile.navigationtest

/**
 * A helper class to facilitate listing multiple view types in a single recyclerview, and
 * facilitates the usage of DiffUtils
 */
abstract class ListableItem {
    abstract val itemViewType: Int
    abstract val item: Any?

    /**
     * @param item
     * @return Whether or not the item is the "same" item as the other one, so for example,
     * if their IDs are the same. Not if they are exactly equal
     */
    abstract fun isTheSame(item: ListableItem): Boolean

    /**
     * @param oldItem the old item we're comparing to
     * @return the differences between the old item and the new item that onBindViewHolder should know about. If there
     * are multiple changes, feel free to return a Bundle here.
     */
    abstract fun getChangesComparedTo(oldItem: ListableItem): Any?

    /**
     * @return whether or not this is exactly equal to item.
     */
    fun equals(item: ListableItem): Boolean {
        return item.javaClass == javaClass
    }
}